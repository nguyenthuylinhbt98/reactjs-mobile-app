import React from 'react'
import Slider from "react-slick";
//neu bi trung ten component thi:
//import SliderSlick from "react-slick";
// thay ten k anh huong

function Slide() {
    return (
        <>
            <div id="slide" className="carousel slide" data-ride="carousel">
               
                <Slider >
                    <div className="carousel-item active">
                    <img src="images/slide-1.png" alt="Vietpro Academy" />
                    </div>
                    <div className="carousel-item">
                    <img src="images/slide-2.png" alt="Vietpro Academy" />
                    </div>
                    <div className="carousel-item">
                    <img src="images/slide-3.png" alt="Vietpro Academy" />
                    </div>
                    <div className="carousel-item">
                    <img src="images/slide-4.png" alt="Vietpro Academy" />
                    </div>
                    <div className="carousel-item">
                    <img src="images/slide-5.png" alt="Vietpro Academy" />
                    </div>
                    <div className="carousel-item">
                    <img src="images/slide-6.png" alt="Vietpro Academy" />
                    </div>
                </Slider>
               
                </div>

        </>
    )
}

export default Slide
