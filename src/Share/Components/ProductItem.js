import React from 'react'
import {getImageProduct} from "./../Untils"
import {formatNumber} from "./../Untils"
import {
    Link, 
    Redirect
  } from "react-router-dom";
function ProductItem({item}) {
    return (
        <>

                <div className="product-item card text-center">
                    <Link to={`/product-detail-${item._id}`}>
                        <img src={getImageProduct(item.image)} />
                    </Link>
                    <h4><Link to={`/product-detail-${item._id}`}>{item.name}</Link></h4>
                    <p>Giá Bán: <span>{formatNumber(item.price)}</span></p>
                </div>
            
        </>
    )
}

export default ProductItem
