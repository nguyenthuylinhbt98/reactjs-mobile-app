import React, { useState } from 'react'
import { Link, useLocation } from "react-router-dom";
function Pagination({pages}) {
    const {total, limit, currentPage, hasPrev, hasNext, prev, next }= pages;
    // console.log(pages)
    const totalPages = Math.ceil(total/limit);
    // console.log('totalPages', totalPages)
    const RenderpageHTML = ()=>{
        const pageHTML = []
        const left = currentPage -2
        const right = currentPage + 2

        for(let i=1; i<= totalPages; i++){
            if( i === 1
                || i=== currentPage 
                || i === totalPages 
                || (i>= left && i<=right)
            ){
                pageHTML.push(i)
                
            }
        }
        return pageHTML
    }
    const {pathname, search} = useLocation()
    // console.log(pathname)
    const query = new URLSearchParams(search)
    // console.log(query)
    const FormatURL = (pages)=>{
        query.set("page", pages)
        return `${pathname}?${query.toString()}`
    }
    return (
        <>
            <div id="pagination">
                    <ul className="pagination">
                        {
                            hasPrev && (
                                <li className="page-item">
                                    <Link className="page-link" to={FormatURL(currentPage-1)}>Trang trước</Link>
                                </li>

                            )
                        }
                    {
                        RenderpageHTML().map((item)=>{
                            return(
                                <li className={`page-item ${currentPage === item && 'active'}`} key={item}>
                                    <Link  
                                        className="page-link" to={FormatURL(item)}>
                                            {item}
                                    </Link>
                                </li>
                            )
                        })
                    }

                    {
                        hasNext && (
                            <li className="page-item">
                                <Link className="page-link" to={FormatURL(currentPage + 1)}>Trang sau</Link>
                            </li>

                        )
                    }
                    
                    </ul> 
            </div>
            
        </>
    )
}
export default Pagination
