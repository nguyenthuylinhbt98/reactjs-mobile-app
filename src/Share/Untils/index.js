import {BASE_URL} from "./../Const/app"
export const getImageProduct = (imgName)=>
    `${BASE_URL}/assets/uploads/${imgName}`
export const formatNumber = (number= 0) =>{
    return Intl.NumberFormat(
        'vi-VN',
        {style: "currency",
        currency: "VND" }
    ).format(number)
}