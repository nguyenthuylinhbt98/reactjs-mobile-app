import HTTP from "./Http"

export const getProducts = (config) => {
    return HTTP.get("/products", config )
}

export const getProductDetail = (id, config) => {
    return HTTP.get(`/products/${id}`, config);
}

export const getCommentsProduct = (id, config) => {
    return HTTP.get(`/products/${id}/comments`, config )
}

export const getCategories = (config) => {
    return HTTP.get("/categories", config)
}

export const getCategory = (id, config) => {
    return HTTP.get(`/categories/${id}`, config)
}

export const getProductCategory = (id, config) => {
    return HTTP.get(`/categories/${id}/products`, config)
}

export const Order = (data, config) => {
    return HTTP.post('order', data, config)
}