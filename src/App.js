import React, { useState, useEffect } from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link, 
  Redirect
} from "react-router-dom";

// import "moment"

import Header from "./Share/Components/Layout/Header"
import Footer from "./Share/Components/Layout/Footer"
import Menu from "./Share/Components/Layout/Menu"
import SideBar from "./Share/Components/Layout/SideBar"
import Slide from "./Share/Components/Layout/Slide"
import Home from "./Page/Home/Index"
import ProductDetail from "./Page/ProductDetail/ProductDetail"
import Cart from "./Page/Cart/Cart"
import Category from "./Page/Category/Category"
import Search from "./Page/Search/Search"
import Success from "./Page/Success/Success"
import NotFound from "./Page/NotFound"
import OrderSuccess from "./Page/OrderSuccess/Index"
import {getCategories} from "./Services/Api"
import store from "./Redux-setup/Store"
import {Provider} from "react-redux"

function App() {
  const [Categories, setCategories] = useState([])
  useEffect(()=>{
    getCategories().then(({data})=>{
      // localStorage.setItem("", JSON.stringify(data.data.docs))
      // console.log("cate",JSON.parse(localStorage.getItem("Categories")))
      // localStorage.removeItem('Categories')
      setCategories(data.data.docs)
    })

    // dong bo bang tay (cach 2 dung thu vien persis)
    // const items = localStorage.getItem("cart_items")
    // store.dispatch({

    //   type:"Action_load",
    //   payload: JSON.parse(items)

    // })
  }, [])

  return (
    <Provider store={store}>
    <Router>
      <Header/>


      <div id="body">
        <div className="container">
          <Menu data={Categories}/>
          <div class="row">
            <div id="main" class="col-lg-8 col-md-12 col-sm-12">
              <Slide/>
              <Switch>
                <Route exact path="/" component={Home}/>

                <Route exact 
                  path="/product-detail-:id" 
                  component={ProductDetail}/>

                <Route exact 
                  path="/cart" 
                  component={Cart}/>

                <Route exact 
                  path="/search" 
                  component={Search}/>

                <Route exact 
                  path="/success" 
                  component={Success}/>

                <Route exact 
                  path="/category-:id" 
                  component={Category}/> 

                <Route exact 
                  path="/order-success" 
                  component={OrderSuccess}/> 

                <Route exact 
                  path="/404" 
                  component={NotFound}/>

                <Route render={
                        ()=><Redirect to="/404"></Redirect>
                      }/> 

              </Switch>
            </div>
            <SideBar/>
          </div>
           
        </div>  
      </div>
     
      <Footer/>
    </Router>
    </Provider>
  )
}

export default App
