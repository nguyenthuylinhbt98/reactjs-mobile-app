import {ADD_TO_CART, UPDATE_CART, DELETE_ITEM_CART, RESET_CART} from "./../../Share/Const/action-type"

const initState = {
    items:[],
}

export default function(state = initState, action){
    switch (action.type){
        case ADD_TO_CART:
            return addItem(state, action.payload)
        // case "Action_load":
        //     return {...state, items: action.payload}
        case UPDATE_CART:
            return updateCart(state, action.payload)
        case DELETE_ITEM_CART:
            const newCart = state.items.filter(
                (item) =>item._id !=action.payload.id
            )
            return {...state, items: newCart}
        case RESET_CART:
            return initState
                
        default:
            return state
    }
}

function addItem(state, payload){
    const items = state.items
    let checkProduct = false
    items.map((item)=>{
        if(!checkProduct && payload._id === item._id){
            item.qty += payload.qty;
            checkProduct = true
        }
        return item
    })

    const newItem = checkProduct ? items : [...items, payload]
    // localStorage.setItem("cart_items", JSON.stringify(newItem))
    return {...state, items: newItem}
}

function updateCart(state, payload){
    const {id, qty}= payload;
    const carts = state.items

    const newCarts = carts.map((item)=>{
        if(item._id === id){
            item.qty = qty;
        }
        return item
    })
    return {...state, items: newCarts}

}