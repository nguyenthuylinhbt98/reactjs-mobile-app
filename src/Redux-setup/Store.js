import {createStore} from "redux"
import reducers from "./Reducers"
import { composeWithDevTools } from 'redux-devtools-extension';
import { persistStore, persistReducer} from "redux-persist"
import storage from 'redux-persist/lib/storage'
// persist : booj nhows luu tru gio hang 
const persistConfig  = {
    key: "redux-store",
    storage: storage
}

const store = createStore(
    persistReducer(persistConfig, reducers), 
    composeWithDevTools())

persistStore(store)
export default store