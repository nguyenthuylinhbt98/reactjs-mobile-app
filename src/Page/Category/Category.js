import React, { useEffect, useState } from 'react'
import {getCategory, getProductCategory} from "./../../Services/Api"
import ProductItem from "./../../Share/Components/ProductItem"
import Pagination from "./../../Share/Components/Pagination"
import ProductItemLoading from "./../../Share/Components/Loading/ProductItemLoading"

function Category(props) {
    const id = props.match.params.id
    const query = new URLSearchParams(props.location.search);
    const page = query.get('page') || 1

    const [category, setCategory] = useState(null)
    const [products, setProduct] = useState([])
    const [totalProduct, setTotal] = useState(0)
    const [pages, setPages] = useState({
        total: 0, 
        limit: 12,
        currentPage: page,
    })
    const [loading, setLoading] = useState({
        loadingCart: false,
    })

    // console.log(pages)
    useEffect(()=>{
        setLoading({...loading, 
            loadingCart: true,
        })
        getCategory(id)
            .then(({data})=>{
                setLoading({...loading, 
                    loadingCart: false,
                })
                if(data && !data.data) return props.history.push("/404")
                setCategory(data.data)

            })
            .catch(()=>{
                props.history.push("/404")
            })

        getProductCategory(id, 
            {params:{
                limit: 12,
                page: page
            }})
            .then(({data})=>{
                console.log("getProductCategory",data)
                setProduct(data.data.docs)
                setTotal(
                    data?.data?.items?.total || 0,
                )
                setPages({... pages , ...data.data.pages})
            })
           
    }, [id, props.history, page])


    return (
        <>
            {/*	List Product	*/}
            <div className="products">
                <h3>{category?.name} (hiện có {totalProduct} sản phẩm)</h3>
                <div className="product-list card-deck">
                    {
                        !loading?.loadingCart
                        ?(products.map((item)=>{
                            return <ProductItem key={item._id} item={item}/>
                        }))
                        :(<ProductItemLoading/>)
                    }
                
               </div>
            </div>
            {/*	End List Product	*/}
            <Pagination
                pages = {pages}
            />            
        </>
    )
}



export default Category

