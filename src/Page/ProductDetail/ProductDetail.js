import React, { useEffect, useState } from 'react'
import {useHistory} from "react-router-dom"
import {getCommentsProduct, getProductDetail} from "./../../Services/Api"
import {ADD_TO_CART} from "./../../Share/Const/action-type"
import {useDispatch} from "react-redux"
import {formatNumber} from "./../../Share/Untils"
function ProductDetail(props) {
    const id = props.match.params.id
    const history = useHistory();

    const dispatch = useDispatch()
    const [comments, setComments] = useState([])
    const [productDetail, setDetail] = useState({})
    useEffect(()=>{
        getProductDetail(id).then(({data})=>{
            setDetail(data.data)
        })
        getCommentsProduct(id).then(({data})=>{
            setComments(data.data.docs)
        })
    }, [])

    function addToCart(type){
        if(productDetail){
            const {_id, name, image, price} = productDetail
            dispatch({
                type: ADD_TO_CART,
                // payload: tên tự đặt, gọi bên store: action.payload
                payload: {
                    _id, 
                    name,
                    image, 
                    price,
                    qty: 1
                }
            })
        }

        if("buy-now" === type){
            history.push("/cart")
        }
        
    }
    return (
        <>
            {/*	List Product	*/}
            <div id="product">
                <div id="product-head" className="row">
                <div id="product-img" className="col-lg-6 col-md-6 col-sm-12">
                    <img src="images/product-1.png" />
                </div>
                <div id="product-details" className="col-lg-6 col-md-6 col-sm-12">
                    <h1>{productDetail.name}</h1>
                    <ul>
                    <li><span>Bảo hành:</span> 12 Tháng</li>
                    <li><span>Đi kèm:</span> {productDetail.accessories}</li>
                    <li><span>Tình trạng:</span> {productDetail.status}</li>
                    <li><span>Khuyến Mại:</span> {productDetail.promotion}</li>
                    <li id="price">Giá Bán (chưa bao gồm VAT)</li>
                    <li id="price-number">{formatNumber(productDetail.price)} đ</li>
                    
                    <li id="status">Còn hàng</li>
                    </ul>
                    <div id="add-cart">
                        {/* <a href="#">Mua ngay</a> */}
                        <button onClick  = {()=>addToCart("buy-now")} className="btn btn-danger">Mua ngay</button>
                        <button onClick={addToCart} className="btn btn-infor">Thêm vào giỏ hàng</button>
                    </div>
                </div>
                </div>
                <div id="product-body" className="row">
                <div className="col-lg-12 col-md-12 col-sm-12">
                    <h3>Đánh giá về {productDetail.name}</h3>
                    {productDetail.details}
                </div>
                </div>
                {/*	Comment	*/}
                <div id="comment" className="row">
                <div className="col-lg-12 col-md-12 col-sm-12">
                    <h3>Bình luận sản phẩm</h3>
                    <form method="post">
                    <div className="form-group">
                        <label>Tên:</label>
                        <input name="comm_name" required type="text" className="form-control" />
                    </div>
                    <div className="form-group">
                        <label>Email:</label>
                        <input name="comm_mail" required type="email" className="form-control" id="pwd" />
                    </div>
                    <div className="form-group">
                        <label>Nội dung:</label>
                        <textarea name="comm_details" required rows={8} className="form-control" defaultValue={""} />     
                    </div>
                    <button type="submit" name="sbm" className="btn btn-primary">Gửi</button>
                    </form> 
                </div>
                </div>
                {/*	End Comment	*/}  
                {/*	Comments List	*/}
                <div id="comments-list" className="row">
                <div className="col-lg-12 col-md-12 col-sm-12">

                    {
                        comments.map((comment)=>{
                            return(
                                <div className="comment-item">
                                    <ul>
                                        <li><b>{comment.name}</b></li>
                                        <li>{comment.updatedAt}</li>
                                        <li>
                                        <p>{comment.content}</p>
                                        </li>
                                    </ul>
                                </div>
                            )

                        })
                    }
                    
                    
                    
                </div>
                </div>
                {/*	End Comments List	*/}
            </div>
            {/*	End Product	*/} 
            <div id="pagination">
                <ul className="pagination">
                <li className="page-item"><a className="page-link" href="#">Trang trước</a></li>
                <li className="page-item active"><a className="page-link" href="#">1</a></li>
                <li className="page-item"><a className="page-link" href="#">2</a></li>
                <li className="page-item"><a className="page-link" href="#">3</a></li>
                <li className="page-item"><a className="page-link" href="#">Trang sau</a></li>
                </ul> 
            </div>
        </>
    )
}

export default ProductDetail
