import React, { useState } from 'react'
import {useSelector} from "react-redux"
import {getImageProduct} from "./../../Share/Untils"
import {ADD_TO_CART, UPDATE_CART, DELETE_ITEM_CART} from "./../../Share/Const/action-type"
import {useDispatch} from "react-redux"
import {formatNumber} from "./../../Share/Untils"
import {Order} from "./../../Services/Api"


// hoac useStore

function Cart( {history}) {
    const dispatch = useDispatch()

    const {carts} = useSelector(({Cart})=>({carts: Cart.items}))
    const [inputs , setInput] = useState({})
    // console.log(carts)
    const TotalPrice = carts.reduce((a,c)=>a+parseInt(c.qty)*parseInt(c.price), 0)
    function onChangeInput(e, id){
        const value = parseInt(e?.target?.value)
        if(value <= 0){
            // eslint-disable-next-line no-restricted-globals
            const isConfirm = confirm("Xoa san pham khoi gio hang")
            return !isConfirm 
            ? dispatch({
                type: UPDATE_CART,
                payload:{
                    id, 
                    qty: 1
                }
            })
            : dispatch({
                type: DELETE_ITEM_CART,
                payload:{
                    id, 
                }
            })

        }

        dispatch({
            type: UPDATE_CART,
            payload:{
                id, 
                qty: parseInt(value)
            }

        })
        
    }


    function onChangeOrderInput(e){
        const {value, name} = e.target
        setInput({...inputs, [name]:value})
    }
    function onClickOrder(e){
        e.preventDefault()
        // const data = inputs
        const items = carts.map(item=>({prd_id: item._id, qty: item.qty}))
        Order({
            items, 
            ...inputs
        }).then(({data})=>{
            if(data.status === "success"){
                history.push("/order-success", { isOrderSuccess: true })
            }
           
        })
    }
    function deleteItem(id){
        // eslint-disable-next-line no-restricted-globals
        const isConfirm = confirm("Xoa san pham khoi gio hang")
        dispatch({
            type: DELETE_ITEM_CART,
            payload:{
                id, 
            }
        })
    }
    return (
        <>
            <div>
                {/*	Cart	*/}
                <div id="my-cart">
                    <div className="row">
                    <div className="cart-nav-item col-lg-7 col-md-7 col-sm-12">Thông tin sản phẩm</div> 
                    <div className="cart-nav-item col-lg-2 col-md-2 col-sm-12">Tùy chọn</div> 
                    <div className="cart-nav-item col-lg-3 col-md-3 col-sm-12">Giá</div>    
                    </div>  
                    <form method="post">
                        {
                            carts.map((item)=>{
                                return(
                                    <div className="cart-item row">
                                        <div className="cart-thumb col-lg-7 col-md-7 col-sm-12">
                                        <img src={getImageProduct(item?.image)} />
                                        <h4>{item.name}</h4>
                                        </div> 
                                        <div className="cart-quantity col-lg-2 col-md-2 col-sm-12">
                                        <input 
                                            onChange={(e)=>onChangeInput(e, item?._id)}
                                            type="number" 
                                            id="quantity" 
                                            className="form-control form-blue quantity" 
                                            value={item.qty}  />
                                        </div> 
                                        <div className="cart-price col-lg-3 col-md-3 col-sm-12">
                                            <b>{formatNumber(item.price)}</b>
                                            <a onClick={(e)=>deleteItem(item?._id)}>Xóa</a>
                                        </div>    
                                    </div>  
                                )
                            })
                        }
                   
                    
                    <div className="row">
                        <div className="cart-thumb col-lg-7 col-md-7 col-sm-12">
                        <button id="update-cart" className="btn btn-success" type="submit" name="sbm">Xóa toàn bộ</button>	
                        </div> 
                        <div className="cart-total col-lg-2 col-md-2 col-sm-12"><b>Tổng cộng:</b></div> 
                        <div className="cart-price col-lg-3 col-md-3 col-sm-12"><b>{formatNumber(TotalPrice)}</b></div>
                    </div>
                    </form>
                </div>
                {/*	End Cart	*/}
                {/*	Customer Info	*/}
                {
                    (carts.length && (
                        <div id="customer">
                            <form method="post">
                            <div className="row">
                                <div 
                                    id="customer-name" 
                                    className="col-lg-4 col-md-4 col-sm-12">
                                <input 
                                    placeholder="Họ và tên (bắt buộc)" 
                                    type="text" 
                                    onChange={onChangeOrderInput}
                                    value={inputs.name}
                                    name="name" 
                                    className="form-control" required />
                                </div>
                                <div id="customer-phone" className="col-lg-4 col-md-4 col-sm-12">
                                <input 
                                    placeholder="Số điện thoại (bắt buộc)" 
                                    type="text" 
                                    name="phone" 
                                    className="form-control" 
                                    onChange={onChangeOrderInput}
                                    value={inputs.phone}
                                    required />
                                </div>
                                <div id="customer-mail" className="col-lg-4 col-md-4 col-sm-12">
                                <input 
                                    placeholder="Email (bắt buộc)" 
                                    type="text" 
                                    name="email" 
                                    className="form-control" 
                                    onChange={onChangeOrderInput}
                                    value={inputs.email}
                                    required />
                                </div>
                                <div id="customer-add" className="col-lg-12 col-md-12 col-sm-12">
                                <input 
                                    placeholder="Địa chỉ nhà riêng hoặc cơ quan (bắt buộc)" 
                                    type="text" 
                                    name="address" 
                                    className="form-control" 
                                    onChange={onChangeOrderInput}
                                    value={inputs.address}
                                    required />
                                </div>
                            </div>
                            </form>
                            <div className="row">
                            <div className="by-now col-lg-6 col-md-6 col-sm-12">
                                <a onClick={onClickOrder}>
                                <b>Mua ngay</b>
                                <span>Giao hàng tận nơi siêu tốc</span>
                                </a>
                            </div>
                            <div className="by-now col-lg-6 col-md-6 col-sm-12">
                                <a href="#">
                                <b>Trả góp Online</b>
                                <span>Vui lòng call (+84) 0988 550 553</span>
                                </a>
                            </div>
                            </div>
                        </div>
                    ))|| null
                }
                
                </div>
        </>
    )
}

export default Cart
