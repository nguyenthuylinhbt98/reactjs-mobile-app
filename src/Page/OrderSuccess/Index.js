import React, { useEffect } from 'react'
import { Redirect } from "react-router-dom";
import {useDispatch} from "react-redux"
import {RESET_CART} from "./../../Share/Const/action-type"
function Index(props) {
    // console.log(props)
    const isOrderSuccess = props?.location?.state?.isOrderSuccess
    console.log("isOrderSuccess",isOrderSuccess)
    const dispatch = useDispatch()
    useEffect(()=>{
        dispatch({
            type: RESET_CART
        })
    }, [dispatch])
    if(!isOrderSuccess){
         return <Redirect to="/"/> 

    }
    return (
        <>
            <div className="alert alert-succes"> Dat hang thanh cong !</div>
        </>
    )
}

export default Index
