import React, { useState } from 'react'
import {getProducts} from "./../../Services/Api"
import { useEffect } from 'react';
import ProductItem from "./../../Share/Components/ProductItem"
import Pagination from "./../../Share/Components/Pagination"

function Search(props) {
    // su dung duocj props cuar route do duoc bao boi route
    // neu k duoc bao can sur dungj hook 
    // console.log(props)
    const query = new URLSearchParams(props.location.search);
    const q = query.get("q")
    const page = query.get('page') || 1
    const [products, setProduct] = useState([])
    const [pages, setPages] = useState({
        total: 0, 
        limit: 12,
        currentPage: page,
    })
    useEffect(()=>{
        getProducts({params:{
            name: q, 
            limit: 12,
            page: page
        }}).then(({data})=>{
            setProduct(data.data.docs)
            setPages({... pages , ...data.data.pages})
        })
    }, [q, page])


    return (
        <>
            <div>
                {/*	List Product	*/}
                <div className="products">
                    <div id="search-result">Kết quả tìm kiếm với sản phẩm <span>iPhone Xs Max 2 Sim - 256GB</span></div>
                    <div className="product-list card-deck">
                        {
                            products.map((item)=>{
                                return <ProductItem key={item._id} item={item}/>
                            })
                        }
                    </div>
                </div>
                {/*	End List Product	*/}

                <Pagination
                    pages = {pages}
                
                />
                </div>

            
        </>
    )
}

export default Search
